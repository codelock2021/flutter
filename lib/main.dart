// ignore_for_file: prefer_const_constructors

import 'package:chart_app/SignInScreen/SignInScreen.dart';
import 'package:chart_app/signUpScreens/AcountSecureScreen.dart';
import 'package:chart_app/signUpScreens/SignUpScreen.dart';
import 'package:chart_app/splashScreen/SplashScreen.dart';

import 'Home/HomeScreen.dart';
import 'Home/WatchListScreen.dart';
import 'package:flutter/material.dart';

import 'signUpScreens/twoStepVerificationScreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: SplashScreen(),
    );
  }
}
