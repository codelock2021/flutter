// ignore_for_file: file_names, use_key_in_widget_constructors, prefer_const_constructors

import 'package:chart_app/SignInScreen/SignInScreen.dart';
import 'package:chart_app/signUpScreens/SignUpScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF1C1C28),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.width * 0.04,
              horizontal: MediaQuery.of(context).size.width * 0.03),
          child: Container(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Spacer(
                  flex: 8,
                ),
                Container(
                  height: MediaQuery.of(context).size.width / 1.5,
                  child: Stack(
                    children: [
                      Positioned(
                        bottom: 20,
                        right: MediaQuery.of(context).size.width / 4,
                        child: Image.asset(
                          'assets/images/png/image_0013 1.png',
                          height: MediaQuery.of(context).size.width / 1.5,
                        ),
                      ),
                      Positioned(
                        bottom: 20,
                        left: MediaQuery.of(context).size.width / 4,
                        child: Image.asset(
                          'assets/images/png/image_0021 1.png',
                          height: MediaQuery.of(context).size.width / 1.5,
                        ),
                      ),
                    ],
                  ),
                ),
                Text(
                  'Welcome to \ncoinshred',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 28,
                      color: Colors.white),
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Your best crypto wallet partner!',
                  style: TextStyle(fontSize: 18, color: Color(0xffBFBFBF)),
                  textAlign: TextAlign.center,
                ),
                Spacer(
                  flex: 4,
                ),
                MaterialButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SignUpScreen(),
                        ));
                  },
                  minWidth: MediaQuery.of(context).size.width / 1.5,
                  color: Color(0xff3586FF),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Text(
                      'Get Started',
                      style: TextStyle(color: Color(0xffFFFFFF), fontSize: 20),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SignInScreen(),
                        ));
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Text(
                      'Sign in',
                      style: TextStyle(color: Color(0xffFFFFFF), fontSize: 20),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
