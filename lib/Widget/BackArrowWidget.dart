// ignore_for_file: file_names

import 'package:flutter/material.dart';

class BackArrowWidget extends StatelessWidget {
  const BackArrowWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: GestureDetector(
        onTap: () {
          Navigator.pop(context);
          print('pop');
        },
        child: Container(
          decoration: BoxDecoration(
              color: Color(0xff2B2B3D),
              borderRadius: BorderRadius.circular(15)),
          child: Padding(
            padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.05),
            child: Image.asset('assets/images/png/Arrow - Right.png'),
          ),
        ),
      ),
    );
  }
}
