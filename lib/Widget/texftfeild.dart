// import 'package:flutter/material.dart';
//
// typedef OnValidate = Function(String);
//
// class CommonTextFeild extends StatelessWidget {
//   final TextEditingController controller;
//   final String? hint;
//   final OnValidate validat;
//   CommonTextFeild(
//       {required this.hint, required this.controller, required this.validat});
//   @override
//   Widget build(BuildContext context) {
//     return TextFormField(
//       controller: controller,
//       validator: validat,
//       cursorColor: Color(0xff3586FF),
//       decoration: InputDecoration(
//         hintText: hint,
//         hintStyle: TextStyle(color: Color(0xff66687F)),
//         // border: OutlineInputBorder(
//         //     borderSide:
//         //         BorderSide(color: Color(0xff66687F), width: 5),
//         //     borderRadius: BorderRadius.circular(20)),
//         focusedBorder: OutlineInputBorder(
//             borderSide: BorderSide(color: Color(0xff3586FF)),
//             borderRadius: BorderRadius.circular(20)),
//         enabledBorder: OutlineInputBorder(
//             borderSide: BorderSide(
//               color: Color(0xff66687F),
//             ),
//             borderRadius: BorderRadius.circular(20)),
//       ),
//     );
//   }
// }
