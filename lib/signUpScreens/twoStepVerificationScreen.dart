// ignore_for_file: file_names, use_key_in_widget_constructors, prefer_const_constructors

import 'package:chart_app/Widget/BackArrowWidget.dart';
import 'package:chart_app/signUpScreens/varificationScreen.dart';
import 'package:flutter/material.dart';

class TwoStepVarification extends StatefulWidget {
  @override
  State<TwoStepVarification> createState() => _TwoStepVarificationState();
}

class _TwoStepVarificationState extends State<TwoStepVarification> {
  TextEditingController number = TextEditingController();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xFF1C1C28),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.width * 0.04,
              horizontal: MediaQuery.of(context).size.width * 0.03),
          child: Form(
            key: formkey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BackArrowWidget(),
                SizedBox(
                  height: 8,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: Text(
                    'Set up 2-Step Verfication',
                    style: TextStyle(
                        color: Color(0xffFFFFFF),
                        fontSize: 22,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: Text(
                    'Enter your phone number so we can text you and authentication code',
                    style: TextStyle(
                        color: Color(0xffBFBFBF),
                        fontSize: 18,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.width * 0.015),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(
                          color: Color(0xff66687F),
                        )),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Row(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: Image.asset(
                                  'assets/images/png/flag-Stars-and-Stripes-May-1-1795 1.png',
                                ),
                              ),
                              Icon(
                                Icons.expand_more,
                                color: Colors.white,
                              )
                            ],
                          ),
                          Expanded(
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Enter Mobile Number';
                                } else if (value.characters.length != 10) {
                                  return 'Enter 10 Digits';
                                }
                              },
                              controller: number,
                              textInputAction: TextInputAction.done,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  hintText: 'Mobile Number',
                                  hintStyle:
                                      TextStyle(color: Color(0xff66687F)),
                                  border: UnderlineInputBorder(
                                      borderSide: BorderSide(width: 0)),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(width: 0))),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Spacer(),
                Align(
                  alignment: Alignment.center,
                  child: MaterialButton(
                    onPressed: () {
                      if (formkey.currentState!.validate()) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Otp(),
                            ));
                        print('valid');
                      } else {
                        print('inValid');
                      }
                    },
                    minWidth: MediaQuery.of(context).size.width / 1.5,
                    color: Color(0xff3586FF),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        'Continue',
                        style:
                            TextStyle(color: Color(0xffFFFFFF), fontSize: 20),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
