// ignore_for_file: file_names, use_key_in_widget_constructors, prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:chart_app/Widget/BackArrowWidget.dart';
import 'package:chart_app/signUpScreens/AcountSecureScreen.dart';
import 'package:flutter/material.dart';

class SignUpScreen extends StatefulWidget {
  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  bool obsecure = true;
  bool checkbox = true;

  TextEditingController name = TextEditingController();

  TextEditingController lname = TextEditingController();

  TextEditingController email = TextEditingController();

  TextEditingController pass = TextEditingController();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> globalKey = GlobalKey<ScaffoldState>();

  Function? CreateAccount() {
    if (formkey.currentState!.validate()) {
      checkbox
          ? Navigator.push(context, MaterialPageRoute(
              builder: (context) {
                return AccountSecureScreen();
              },
            ))
          : globalKey.currentState!.showSnackBar(SnackBar(
              content: Text(
              'Please Agree Privacy & Policy',
            )));
    } else {
      print('not valid');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: globalKey,
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xFF1C1C28),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.width * 0.04,
              horizontal: MediaQuery.of(context).size.width * 0.03),
          child: Form(
            key: formkey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BackArrowWidget(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Create a New \nAccount',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: MediaQuery.of(context).size.width * 0.08),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.width * 0.01,
                      width: MediaQuery.of(context).size.width * 0.1,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Color(0xff3586FF),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.width * 0.015),
                  child: TextFormField(
                    controller: name,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Enter Name';
                      }
                    },
                    cursorColor: Color(0xff3586FF),
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      hintText: 'Name',
                      hintStyle: TextStyle(color: Color(0xff66687F)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xff3586FF)),
                          borderRadius: BorderRadius.circular(20)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0xff66687F),
                          ),
                          borderRadius: BorderRadius.circular(20)),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.width * 0.015),
                  child: TextFormField(
                    controller: lname,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Enter Last Name';
                      } else {
                        return null;
                      }
                    },
                    cursorColor: Color(0xff3586FF),
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      hintText: 'Last Name',
                      hintStyle: TextStyle(color: Color(0xff66687F)),
                      // border: OutlineInputBorder(
                      //     borderSide:
                      //         BorderSide(color: Color(0xff66687F), width: 5),
                      //     borderRadius: BorderRadius.circular(20)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xff3586FF)),
                          borderRadius: BorderRadius.circular(20)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0xff66687F),
                          ),
                          borderRadius: BorderRadius.circular(20)),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.width * 0.015),
                  child: TextFormField(
                    controller: email,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Enter Email Address';
                      } else {
                        return null;
                      }
                    },
                    cursorColor: Color(0xff3586FF),
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      hintText: 'Email Address',
                      hintStyle: TextStyle(color: Color(0xff66687F)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xff3586FF)),
                          borderRadius: BorderRadius.circular(20)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0xff66687F),
                          ),
                          borderRadius: BorderRadius.circular(20)),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.width * 0.015),
                  child: TextFormField(
                    controller: pass,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Enter Password';
                      } else {
                        return null;
                      }
                    },
                    obscureText: obsecure,
                    cursorColor: Color(0xff3586FF),
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() {
                            if (obsecure == false) {
                              obsecure = true;
                            } else {
                              obsecure = false;
                            }
                          });
                        },
                        child: obsecure
                            ? Icon(
                                Icons.visibility_off,
                                color: Colors.white,
                              )
                            : Icon(
                                Icons.remove_red_eye,
                                color: Colors.white,
                              ),
                      ),
                      hintText: 'Password',
                      hintStyle: TextStyle(color: Color(0xff66687F)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xff3586FF)),
                          borderRadius: BorderRadius.circular(20)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color(0xff66687F),
                          ),
                          borderRadius: BorderRadius.circular(20)),
                    ),
                  ),
                ),
                Spacer(),
                Row(
                  children: [
                    Checkbox(
                      onChanged: (val) {
                        setState(() {
                          checkbox = !checkbox;
                        });
                        print(checkbox.toString());
                      },
                      value: checkbox,
                      focusColor: Color(0xff3586FF),
                    ),
                    Text(
                      'I certify that I’m 18 years of age or older, and I agree to\n the User Agreement and Privacy Policy',
                      style: TextStyle(
                        color: Color(0xffFFFFFF),
                      ),
                    ),
                  ],
                ),
                Align(
                  alignment: Alignment.center,
                  child: MaterialButton(
                    onPressed: CreateAccount,
                    minWidth: MediaQuery.of(context).size.width / 1.5,
                    color: Color(0xff3586FF),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                        'Create Account',
                        style:
                            TextStyle(color: Color(0xffFFFFFF), fontSize: 20),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
