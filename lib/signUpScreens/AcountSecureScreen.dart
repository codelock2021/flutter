// ignore_for_file: file_names, use_key_in_widget_constructors, prefer_const_constructors

import 'package:chart_app/Widget/BackArrowWidget.dart';
import 'package:chart_app/signUpScreens/twoStepVerificationScreen.dart';
import 'package:flutter/material.dart';

class AccountSecureScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF1C1C28),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.width * 0.04,
              horizontal: MediaQuery.of(context).size.width * 0.03),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Align(alignment: Alignment.topLeft, child: BackArrowWidget()),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Image.asset(
                  'assets/images/png/031.png',
                  height: MediaQuery.of(context).size.width / 1.5,
                ),
              ),
              Text(
                'Let’s Secure Your Account',
                style: TextStyle(
                    color: Color(0xffFFFFFF),
                    fontSize: 22,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                'We sent a verification Phone. One way keep account secure with 2-step verification, which requires phone number.',
                style: TextStyle(
                    color: Color(0xffBFBFBF),
                    fontSize: 18,
                    fontWeight: FontWeight.w500),
                textAlign: TextAlign.center,
              ),
              Spacer(),
              MaterialButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TwoStepVarification(),
                      ));
                },
                minWidth: MediaQuery.of(context).size.width / 1.5,
                color: Color(0xff3586FF),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    'Start',
                    style: TextStyle(color: Color(0xffFFFFFF), fontSize: 20),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
