// ignore_for_file: file_names, prefer_const_constructors
import 'chart.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);

  List<Map<String, dynamic>> watchlist = [
    {
      'name': 'Bitcoin',
      'icon': 'assets/images/png/Vector.png',
      'color': Color(0xffFDAB1D)
    },
    {
      'name': 'Dash',
      'icon': 'assets/images/png/Vector (1).png',
      'color': Color(0xff3B7ABE)
    },
    {
      'name': 'Etherium',
      'icon': 'assets/images/png/Group.png',
      'color': Color(0xff39D98A)
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF1C1C28),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.width * 0.04,
                  horizontal: MediaQuery.of(context).size.width * 0.03),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        radius: MediaQuery.of(context).size.width / 12,
                        backgroundColor: Color(0xffFDAB1D),
                        // child: Image.asset(
                        //   'assets/images/png/081.png',
                        //   fit: BoxFit.cover,
                        // ),
                        backgroundImage: AssetImage(
                          'assets/images/png/081.png',
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Good Morning',
                            style: TextStyle(
                                color: Color(0xffBFBFBF), fontSize: 18),
                          ),
                          Text(
                            'Daniel Smith',
                            style: TextStyle(
                                color: Color(0xffFFFFFF), fontSize: 22),
                          ),
                        ],
                      ),
                      Spacer(),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Color(0xff2B2B3D)),
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Stack(
                              children: [
                                Image.asset(
                                  'assets/images/png/Notification.png',
                                  height: 35,
                                ),
                                Positioned(
                                    top: 5,
                                    right: 5,
                                    child: CircleAvatar(
                                      radius: 5,
                                      backgroundColor: Color(0xffFDAB1D),
                                    )),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.width * 0.04),
                    child: Container(
                      height: MediaQuery.of(context).size.height / 4,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Color(0xff2B2B3D)),
                      child: Stack(
                        alignment: Alignment.bottomRight,
                        children: [
                          Image.asset('assets/images/png/coinlogo.png'),
                          Positioned(
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 20, horizontal: 15),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Total Portfolio Value',
                                        style: TextStyle(
                                            color: Color(0xffBFBFBF),
                                            fontSize: 16),
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            'USD',
                                            style: TextStyle(
                                                color: Color(0xFFFDAB1D),
                                                fontSize: 18),
                                          ),
                                          Image.asset(
                                            'assets/images/png/Stroke1.png',
                                            height: 20,
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  Text(
                                    '\$6642.20',
                                    style: TextStyle(
                                        fontSize: 26,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xFFFFFFFF)),
                                  ),
                                  Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 8),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: const [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          'Monthly Profit',
                                          style: TextStyle(
                                              color: Color(0xffBFBFBF),
                                              fontSize: 16),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          '+ \$6642.20',
                                          style: TextStyle(
                                              color: Color(0xff39D98A),
                                              fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width * 0.03),
                      child: Text(
                        'Watchlist',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Color(0xffFFFFFf)),
                      ),
                    ),
                    Container(
                        height: MediaQuery.of(context).size.height / 3.7,
                        child: ListView.builder(
                          itemCount: watchlist.length,
                          physics: BouncingScrollPhysics(),
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                width: MediaQuery.of(context).size.width / 2.8,
                                decoration: BoxDecoration(
                                    color: Color(0xff2B2B3D),
                                    borderRadius: BorderRadius.circular(20)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02,
                                          horizontal: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.03),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              CircleAvatar(
                                                radius: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.05,
                                                backgroundColor:
                                                    watchlist[index]['color'],
                                                child: Image.asset(
                                                    watchlist[index]['icon']),
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Text(
                                                watchlist[index]['name'],
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 18,
                                                    color: Color(0xffFFFFFF)),
                                                overflow: TextOverflow.ellipsis,
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Row(
                                            children: const [
                                              Text(
                                                '128.76',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 20,
                                                    color: Color(0xffffffff)),
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Text(
                                                'BTC',
                                                style: TextStyle(
                                                    color: Color(0xffBFBFBF)),
                                                overflow: TextOverflow.ellipsis,
                                              )
                                            ],
                                          ),
                                          Text(
                                            '+18.33 (18%)',
                                            style: TextStyle(
                                                color: Color(0xff3586FF)),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                        child: ClipRRect(
                                            borderRadius: BorderRadius.vertical(
                                                bottom: Radius.circular(20)),
                                            child: homeScreenChart())),
                                  ],
                                ),
                              ),
                            );
                          },
                        )),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width * 0.03),
                      child: Text(
                        'Top Moves',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Color(0xffFFFFFf)),
                      ),
                    ),
                    Column(
                      children: List.generate(
                        7,
                        (index) => Container(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    CircleAvatar(
                                      radius:
                                          MediaQuery.of(context).size.width *
                                              0.08,
                                      child: Image.asset(
                                          'assets/images/png/Vector (2).png'),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: const [
                                        Text(
                                          'Uniswap',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w700,
                                              fontSize: 18,
                                              color: Color(0xffFFFFFF)),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          'UNI',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Color(0xffFFFFFF)),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: const [
                                    Text(
                                      '\$16,351.57',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                          fontSize: 18,
                                          color: Color(0xffFFFFFF)),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      '+0.67%',
                                      style: TextStyle(
                                          fontSize: 16,
                                          color: Color(0xff39D98A)),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
