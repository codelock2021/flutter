// ignore: file_names
// ignore_for_file: file_names, prefer_const_constructors

import 'chart.dart';
import 'package:flutter/material.dart';

class WatchListScreen extends StatelessWidget {
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> detail = [
      {'value': '\$19.283', 'name': 'Global Avg.'},
      {'value': '\$80.8B', 'name': 'Market Cap'},
      {'value': '\$1.46B', 'name': 'Volume'}
    ];
    return Scaffold(
      backgroundColor: Color(0xFF1C1C28),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.width * 0.04,
              horizontal: MediaQuery.of(context).size.width * 0.03),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: Color(0xFF2B2B3D),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Image.asset(
                        'assets/images/png/Arrow - Right.png',
                        height: 22,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Watchlist',
                    style: TextStyle(
                        color: Color(0xFFFFFFFF),
                        fontWeight: FontWeight.w600,
                        fontSize: 18),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 15),
                child: Container(
                  decoration: BoxDecoration(
                      color: Color(0xFF2B2B3D),
                      borderRadius: BorderRadius.circular(20)),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  radius: 25,
                                  backgroundColor: Color(0xFFFDAB1D),
                                  child: Image.asset(
                                      'assets/images/png/Vector.png'),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  'Bitcoin',
                                  style: TextStyle(
                                      color: Color(0xFFFFFFFF), fontSize: 18),
                                ),
                              ],
                            ),
                            Spacer(),
                            Row(
                              children: [
                                Text(
                                  'USD',
                                  style: TextStyle(
                                      color: Color(0xFFFDAB1D), fontSize: 20),
                                ),
                                Image.asset(
                                  'assets/images/png/Stroke1.png',
                                  height: 20,
                                )
                              ],
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          '3.5210.79',
                          style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.w600,
                              color: Color(0xFFFFFFFF)),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          '+\$19.283 USD',
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF39D98A)),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: List.generate(
                      3,
                      (index) => Column(
                            children: [
                              Text(
                                detail[index]['value'],
                                style: TextStyle(
                                    color: Color(0xFFFFFFFF),
                                    fontWeight: FontWeight.w500,
                                    fontSize: 22),
                              ),
                              Text(
                                detail[index]['name'],
                                style: TextStyle(
                                    color: Color(0xFFBFBFBF),
                                    fontWeight: FontWeight.w400,
                                    fontSize: 16),
                              ),
                            ],
                          )),
                ),
              ),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Container(child: LineChartSample2()),
              )),
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: MaterialButton(
                            height: MediaQuery.of(context).size.width / 6,
                            onPressed: () {},
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            color: Color(0xFF3586FF),
                            child: Text(
                              'Sell',
                              style: TextStyle(
                                  color: Color(0xFFFFFFFF),
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: MaterialButton(
                            height: MediaQuery.of(context).size.width / 6,
                            onPressed: () {},
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            color: Color(0xFFD6E7FF),
                            child: Text(
                              'Buy',
                              style: TextStyle(
                                  color: Color(0xFF3586FF),
                                  fontWeight: FontWeight.w600,
                                  fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
